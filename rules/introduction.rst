============
Introduction
============

Dans Ascension Inc. vous incarnez un membre d'une corporation dans un monde
cyberpunk qui tente de s'élever dans les échelons de la hierarchie jusqu'à
obtenir une place au Conseil d'administration.

Licence
=======

L'intégralité du jeu (règles, éléments graphiques ou de contexte) est disponible
sous la licence Art Libre 1.3.

