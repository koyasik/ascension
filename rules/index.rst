.. Ascension documentation master file, created by
   sphinx-quickstart on Thu Nov 17 09:53:15 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Ascension Inc. - Règles du jeu
==============================

Contents:

.. toctree::
   :maxdepth: 2

  introduction
  setup
  turn

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

