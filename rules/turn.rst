###########
Tour de jeu
###########

Début du tour
=============

Au début de son tour le joueur pioche deux cartes du paquet actions. Il reçoit
également les bonus associés à son poste.

Actions
=======

Durant son tour un joueur peut effectuer jusqu'à trois actions parmis :

* Chercher une opération
* Planifier une opération
* Recruter un agent
* Impliquer un agent
* Lancer une opération
* Poursuivre une opération
* Obtenir une promotion

À n'importe quel moment durant son tour ou celui d'un autre joueur, un joueur
peut réaliser l'action suivante :

* Jouer une action

Chercher une opération
----------------------

Le joueur souhaitant planifier une opération doit tirer deux cartes du paquet
"Opérations" face cachée. Il peut ensuite se défausser des deux cartes ou en
conserver une.

Le joueur peut ensuite gratuitement jouer une action "Planifier une opération".

Planifier une opération
-----------------------

Pour planifier une opération le joueur place devant lui face cachée une carte
opération depuis sa main.

Recruter un agent
-----------------

Pour recruter des cartes agent, le joueur tire trois cartes face cachées depuis
le paquet "Agent" et choisir d'en conserver une dans sa main. Les autres cartes
sont placées face visible dans la zone de recrutement.

.. warning::

  La zone de recrutement ne peut contenir que 5 cartes agent. Si la zone de
  recrutement contient déjà 5 cartes, alors le joueur doit en défausser autant
  que nécessaire pour qu'il puisse poser les cartes agent qu'il n'a pas choisi.

À la fin d'un recrutement le joueur peut effectuer gratuitement une action
"Impliquer un agent".

Impliquer un agent
------------------

Pour impliquer un agent le joueur place celui-ci face visible devant lui et doit
payer le coût en pouvoir et en crédits indiqués sur la carte.

Lancer une opération
--------------------

Il est nécessaire d'avoir au préalable planifié une opération avant de pouvoir
la lancer.

Pour lancer une opération, le joueur retourne face visible (si elle est toujours
face cachée) l'opération en attente et applique les effets préliminaires.

.. note::

  Les autres joueurs peuvent interrompre la réalisation de l'opération en jouant
  des cartes actions rendant ainsi le joueur entrain de lancer l'opération
  incapable de terminer l'action durant son tour.

Si la somme de chacune des compétences du joueur est supérieure ou égale aux
prérequis de l'opération, le joueur remporte celle-ci et peut appliquer les
gains de fin d'opération.

Si le joueur n'est pas en mesure de remporter l'opération, il peut soit :

* Tenter de "Poursuivre l'opération" durant son prochain tour
* Abandonner l'opération. Auquel cas le joueur applique immédiatement les
  pénalités d'abandon. Son tour prend fin immédiatement. Si le joueur ne peut
  appliquer les pénalités d'abandon d'opération (probalement parce qu'il ne peut
  pas payer) alors il est rétrogradé.

.. warning::

  Poursuivre une opération a un coût qu'il faut payer à la fin de chaque tour
  durant lequel une opération est en court.

Poursuivre une opération
------------------------

Si un joueur a été contraint de reporter une opération, il peut la reprendre
s'il a de nouveau réuni les compétences. Les mêmes règles que celles de l'action
"Lancer une opération" s'appliquent.

Obtenir une promotion
---------------------

Pour obtenir une promotion un joueur doit dépenser le nombre de crédit et de
points de pouvoir du poste supérieur au sien. Un joueur ne peut pas être promu à
un poste de plusieurs niveaux au dessus du sien. On ne peut pas obtenir plus
d'une promotion par tour.

Fin du tour
===========

À la fin de son tour un joueur ne peut avoir en main plus de 7 cartes. Si c'est
le cas il doit se défausser du nombre de cartes de son choix afin d'atteindre
7 cartes.

