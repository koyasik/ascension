###################################
Description et mise en place du jeu
###################################

Description des éléments
========================

La plupart des éléments du jeu contiennent des règles supplémentaires qui
contredisent parfois les règles générales ou les règles des autres cartes. Pour
savoir qu'elles règles s'appliquent il suffit de respecter l'ordre suivant :

* Les règles de base sont les moins importantes
* Les règles de la planche de poste sont plus importantes que les règles de base
* Les règles des cartes opération sont plus importantes que les règles de la
  planche de poste
* Les règles des cartes agents sont plus importantes que les règles des cartes
  opération
* Les règles des cartes action sont plus importantes que les règles des cartes
  actions

La plupart des cartes contiennent également une illustration et un texte
d'ambiance, mais ceux-ci n'ont aucun impact sur les règles.

Planche et marqueur de poste
----------------------------

La planche et le marqueur de poste servent à matérialiser l'ascension du joueur
dans la hiérarchie de la corporation.

Sur la planche se trouvent également les rappels concernant les gains de crédit
et de pouvoir à chaque tour.

Cartes opération
----------------

Les cartes opérations représentent le cœur d'Ascension. En réussissant des
opérations, les joueurs pourront acquérir le pouvoir et les crédits nécessaires
pour s'élever dans la hiérarchie de la corporation, voire de remporter
instantanément la partie.

Une carte opération est découpée en 3 sections importantes :

* Les effets préliminaires : ils s'appliquent dès qu'un joueur lance
  l'opération. Il s'agit souvent d'un coût en crédits et en pouvoir.
  Le coût d'entretien de l'opération est indiqué ici, c'est le coût à payer à la
  fin de chaque tour ou l'opération est active.
* Les compétences requises : pour réussir une opération il faut réunir
  suffisamment de compétences en combat, gestion et technique. 
* Les gains : réussir une opération permet de remporter les gains indiqués sur
  la carte.

Cartes agent
------------

Les agents sont les personnes qui vont vous aider à atteindre les compétences
requises par les opérations.

Les cartes agents sont découpées en 3 sections :

* Un zone de coût : il s'agit du coup à payer pour pouvoir impliquer l'agent
  dans une opération.
* Une zone de compétences : dans cette zone se trouvent les compétences de
  l'agent qui additionnées à celles des autres agents permet se savoir si le
  joueur peut remporter l'opération en court.
* Une zone de règles spéciales : dans cette zone optionnelle se trouvent parfois
  des règles supplémentaires pour la carte.

Cartes action (nom à changer)
-----------------------------

Les cartes actions représentent des évènements et des objets pouvant aider le
joueur ou malmener ses adversaires. Sauf indications contraire les cartes
actions peuvent être jouées à n'importe quel moment du jeu.

Les cartes actions sont divisées en 2 sections :

* Une zone de coût : il s'agit du coût à payer pour pouvoir jouer la carte
  action.
* Une zone de règles : dans cette zone sont renseignées les règles spéciales de
  la carte action.

Mise en place du jeu
====================

Après avoir placé les trois paquets (Opérations, Agents et Action), chaque
joueur pioche :

* 2 cartes opération
* 2 cartes agent
* 3 cartes action

Placez ensuite le marqueur de poste sur la case "Employé".

Choisissez un joueur pour débuter la partie. Parmi les méthodes de désignation
valides vous pouvez :

* Choisir le dernier vainqueur d'une partie d'Ascension
* Élire quelqu'un en votant à main levée

Vous êtes prêt à démarrer !

